<?php

namespace App;

class UntimedEntity extends Entity
{
    public $timestamps = false;

    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
