<?php

namespace App\Http\Controllers;

use App\Models\IcustomerModel;
use Illuminate\Http\Request;

final class CustomerController extends Controller
{
    /**
     * IndexController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return $this
     */
    final public function register(Request $request)
    {
        $output = self::DEFAULT_HTTP_RESPONSE_OUTPUT;
        $status = self::HTTP_STATUS_BAD_REQUEST;

        try {
            $postedData = $request->input();
            $postedData['status'] = intval($postedData['status'] ?? env(self::ENTITY_STATUS_ACTIVE));
            $customer = new IcustomerModel($postedData);
            $customerExists = $customer->exists([
                IcustomerModel::PREFIX . IcustomerModel::DEFAULT_EXISTS_ATTR,
                $postedData[IcustomerModel::DEFAULT_EXISTS_ATTR]
            ]);

            if ($customerExists === false) {
                $customer->save();
                $status = $this->httpStatusCode[self::HTTP_STATUS_OK];

                $output = [
                    'success' => true,
                    'status' => $status,
                    'message' => 'Hello new customer...!',
                ];
            }
            else {
                $status = $this->httpStatusCode[self::HTTP_STATUS_CONFLICT];
                $output['message'] = IcustomerModel::FRIENDLY_NAME . env(self::HTTP_MESSAGE_CONFLICT);
            }
        }
        catch (\Exception $exception) {
            $this->catchException($exception, $status, $output);
        }
        finally {
            return $this->respond($output, $status);
        }
    }
}
