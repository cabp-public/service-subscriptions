<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

final class IndexController extends Controller
{
    /**
     * IndexController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Default index action
     * @param Request $request
     * @return $this
     */
    final public function index(Request $request)
    {
        $status = 400;
        $output = [
            'success' => false,
            'details' => [],
        ];

        try {
            $status = 200;
            $output = [
                'success' => true,
                'details' => "Hello...",
            ];
        }
        catch (\Exception $exception) {
            $output = [
                'exception' => [
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'message' => $exception->getMessage(),
                ]
            ];

//            $this->exceptionHandler->report($exception);
        }
        finally {
            return $this->respond($output, $status);
        }
    }
}
