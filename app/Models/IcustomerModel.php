<?php

namespace App\Models;

use App\Entity;

/**
 * @property integer $icustomer_id
 * @property integer $icustomer_type
 * @property string $icustomer_code
 * @property integer $icustomer_id_type
 * @property string $icustomer_identification
 * @property string $icustomer_name
 * @property string $icustomer_surename
 * @property integer $icustomer_country
 * @property integer $icustomer_state
 * @property integer $icustomer_city
 * @property string $icustomer_address_shipping
 * @property string $icustomer_address_invoicing
 * @property string $icustomer_email
 * @property string $icustomer_phones
 * @property string $icustomer_creation
 * @property string $icustomer_edition
 * @property integer $icustomer_status
 */
class IcustomerModel extends Entity
{
    const PREFIX = 'icustomer_';
    const FRIENDLY_NAME = 'Customer';
    const DEFAULT_EXISTS_ATTR = 'identification';
    const CREATED_AT = 'icustomer_creation';
    const UPDATED_AT = 'icustomer_edition';

    protected $table = 'icustomer';
    protected $primaryKey = 'icustomer_id';
    protected $fillable = [
        self::PREFIX . 'type',
        self::PREFIX . 'code',
        self::PREFIX . 'id_type',
        self::PREFIX . 'identification',
        self::PREFIX . 'name',
        self::PREFIX . 'surename',
        self::PREFIX . 'country',
        self::PREFIX . 'state',
        self::PREFIX . 'city',
        self::PREFIX . 'address_shipping',
        self::PREFIX . 'address_invoicing',
        self::PREFIX . 'email',
        self::PREFIX . 'phones',
        self::PREFIX . 'status',
    ];
    protected $pricingPlans = [];

    function __construct(array $newAttributes = [])
    {
        $this->prefix = self::PREFIX;
        $attributes = [];

        try {
            if (!empty($newAttributes)) {
                $pricingPlansStr = $newAttributes['pricing_plans'] ?? '';
                $this->pricingPlans = explode(',', $pricingPlansStr);
                unset($newAttributes['pricing_plans']);

                foreach ($this->fillable as $column) {
                    $key = substr($column, strlen($this->prefix));
                    $attributes[$column] = $newAttributes[$key] ?? null;
                }
            }
        }
        catch (\Exception $exception) {
            $this->catchException($exception);
        }
        finally {
            parent::__construct($attributes);
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(
            'App\Models\CustomerSubscriptionsModel',
            'icustomer_id',
            $this->primaryKey
        );
    }
}