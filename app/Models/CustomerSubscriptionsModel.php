<?php

namespace App\Models;

use App\UntimedEntity;

/**
 * @property integer $pricing_plan_id
 * @property integer $icustomer_id
 */
class CustomerSubscriptionsModel extends UntimedEntity
{
    protected $table = 'customer_subscriptions';
    protected $fillable = [
        'pricing_plan_id',
        'icustomer_id',
    ];

    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}