<?php

/**
 * Defines application features from the VISITOR context.
 */
class VisitorContext extends TestContext
{
    private $httpResponse;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->httpResponse = new \Illuminate\Http\Response();
    }

    /**
     * @When Consumer performs a GET request to :apiUrl
     * @param string $apiUri
     */
    public function ConsumerPerformsGetRequest(string $apiUri)
    {
        try {
            $this->httpResponse = $this->get($apiUri)->response;
        }
        catch (\Exception $exception) {
            $this->fail($exception->getMessage());
        }
    }

    /**
     * @Then Consumer gets a :expectedHttpCode response
     * @param $expectedHttpCode
     */
    public function ConsumerGetsHttpResponse($expectedHttpCode)
    {
        $output = false;

        try {
            $output = $this->httpResponse->status() === intval($expectedHttpCode);
        }
        catch (\Exception $exception) {}
        finally {
            $this->assertTrue($output);
        }
    }

}
