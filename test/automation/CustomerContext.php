<?php

/**
 * Defines application features from the CUSTOMER context.
 */
class CustomerContext extends TestContext
{
    private $newCustomer;
    private $httpResponse;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->newCustomer = new \stdClass();
        $this->newCustomer->type = null;
        $this->newCustomer->id_type = null;
        $this->newCustomer->identification = null;
        $this->newCustomer->name = null;
        $this->newCustomer->surename = null;
        $this->newCustomer->country = null;
        $this->newCustomer->state = null;
        $this->newCustomer->city = null;
        $this->newCustomer->address_invoicing = null;
        $this->newCustomer->email = null;
        $this->newCustomer->phones = null;
        $this->newCustomer->service_plans = [];

        $this->httpResponse = new \Illuminate\Http\Response();
    }

    /**
     * @Given /^Customer type is (.*)$/
     * @param int $type
     */
    public function SetCustomerType(int $type)
    {
        try {
            $this->newCustomer->type = $type;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Customer identification type is (.*)$/
     * @param int $id_type
     */
    public function SetCustomerIdType(int $id_type)
    {
        try {
            $this->newCustomer->id_type = $id_type;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Customer identification is (.*)$/
     * @param string $identification
     */
    public function SetCustomerIdentification(string $identification)
    {
        try {
            $this->newCustomer->identification = $identification;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Customer name is (.*)$/
     * @param string $name
     */
    public function SetCustomerName(string $name)
    {
        try {
            $this->newCustomer->name = $name;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Customer surename is (.*)$/
     * @param string $surename
     */
    public function SetCustomerSurename(string $surename)
    {
        try {
            $this->newCustomer->surename = $surename;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Customer country is (.*)$/
     * @param int $country
     */
    public function SetCustomerCountry(int $country)
    {
        try {
            $this->newCustomer->country = $country;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Customer state is (.*)$/
     * @param int $state
     */
    public function SetCustomerState(int $state)
    {
        try {
            $this->newCustomer->state = $state;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Customer city is (.*)$/
     * @param int $city
     */
    public function SetCustomerCity(int $city)
    {
        try {
            $this->newCustomer->city = $city;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Customer invoicing address is (.*)$/
     * @param string $address
     */
    public function SetCustomerInvoicingAddress(string $address)
    {
        try {
            $this->newCustomer->address_invoicing = $address;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Customer email is (.*)$/
     * @param string $email
     */
    public function SetCustomerEmail(string $email)
    {
        try {
            $this->newCustomer->email = $email;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Customer phones are (.*)$/
     * @param string $phones
     */
    public function SetCustomerPhones(string $phones)
    {
        try {
            $this->newCustomer->phones = $phones;
        }
        catch (\Exception $exception) {}
    }

    /**
     * @Given /^Selected pricing plans are (.*)$/
     * @param string $selectedPlans
     */
    public function SetPricingPlans(string $selectedPlans)
    {
        try {
            $this->newCustomer->service_plans = explode(',', trim($selectedPlans));
        }
        catch (\Exception $exception) {}
    }

    /**
     * @When Consumer performs a POST request to :apiUri
     * @param string $apiUri
     */
    public function ConsumerPerformsPostRequest(string $apiUri)
    {
        $newCustomer = json_decode(json_encode($this->newCustomer), true);

        try {
            $this->httpResponse = $this->json('POST', $apiUri, $newCustomer)->response;
        }
        catch (\Exception $exception) {
            $this->fail($exception->getMessage());
        }
    }

    /**
     * @Then Consumer gets a :expectedHttpCode response
     * @param $expectedHttpCode
     */
    public function ConsumerGetsHttpResponse($expectedHttpCode)
    {
        $output = false;
        $content = [];

        try {
            $content = json_decode($this->httpResponse->content(), true);
//            var_dump($content);
            $output = intval($content['status']) === intval($expectedHttpCode);
        }
        catch (\Exception $exception) {
            $this->fail($exception->getMessage());
        }
        finally {
            $message = "\nAssertion Error:\n" . json_encode($content);
            $this->assertTrue($output, $message);
        }
    }
}