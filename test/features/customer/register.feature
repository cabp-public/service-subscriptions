@customer
Feature: Customer Register
  In order to use the AI-BP-00 API
  As a new Customer
  I need to register either as an individual or organization

  Scenario Outline: Register
    Given Customer type is <type>
    And Customer identification type is <id_type>
    And Customer identification is <identification>
    And Customer name is <name>
    And Customer surename is <surename>
    And Customer country is <country>
    And Customer state is <state>
    And Customer city is <city>
    And Customer invoicing address is <address_invoicing>
    And Customer email is <email>
    And Customer phones are <phones>
    And Selected pricing plans are <pricing_plans>
    When Consumer performs a POST request to "/customer/register"
    Then Consumer gets a 200 response

    Examples:
      | pricing_plans | type | id_type | identification | name   | surename | country | state | city | address_invoicing    | email                 | phones       |
      | AI-BP-03      |  2   |  1      | 270-09-3045    | Crissy | McFie    | 1       | 1     | 1    | 55910 Surrey Terrace | aemerton0@nytimes.com | (315) 117011 |
