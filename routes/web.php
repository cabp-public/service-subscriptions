<?php

//$router->get('/', function () use ($router) {
//    return $router->app->version();
//});

// Default
$router->get('/', 'IndexController@index');

// Customer
$router->group(['prefix' => 'customer'], function () use ($router) {
    $router->post('register', 'CustomerController@register');
});
